package exercise3;

import com.sun.nio.sctp.NotificationHandler;
import com.sun.source.tree.BreakTree;

class Amount{

    private String currency;
    private int amount;

    public Amount(String currency, int amount){
        super();
        this.currency = currency;
        this.amount = amount;
    }
    class CurrencyDoNotMatchException extends RuntimeException{
        public CurrencyDoNotMatchException(String msg){
            super(msg);
        }
    }
    public void add(Amount that){
        if(!this.currency.equals(that.currency)){
           // throw new Exception("Currencies are not Same");
            throw new CurrencyDoNotMatchException("Currencies are not Same");
        }
       this.amount = this.amount + that.amount;
    }

    public String toString(){
        return amount + " " + currency;
    }
}
public class ExceptionClass4 {
    public static void main(String[] args) {
        Amount amount1 = new Amount("USD", 10);
        Amount amount2 = new Amount("EUR", 50);
        amount1.add(amount2);
        System.out.println(amount1);
    }

}
