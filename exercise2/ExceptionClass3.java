package exercise2;

import java.util.Scanner;

public class ExceptionClass3 {

    public static void main(String[] args) {
        Scanner scanner = null;
        try {
            scanner = new Scanner(System.in);
            int a[] = {1, 2, 3, 4};
            int num = a[5];

        } catch (Exception e) {
            e.printStackTrace();
        }finally {      // always executed
            System.out.println("Before Scanner close");
            if(scanner != null)
                scanner.close();
        }
        System.out.println("Print");

    }
}
