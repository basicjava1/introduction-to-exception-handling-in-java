package exercise4;

import java.util.Scanner;

public class ExceptionClass5 {

    public static void main(String[] args) {
        try(Scanner sc = new Scanner(System.in);) {
            int[] numbers = {1,23,54,56,3};
            int number = numbers[55];
        }

    }
}
